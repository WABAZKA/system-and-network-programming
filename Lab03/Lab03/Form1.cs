﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab03
{
    public partial class Form1 : Form
    {
        protected readonly PerformanceCounter ramCounter = new PerformanceCounter("Memory", "Available MBytes");
        //счетчик производительности системной памяти
        public Form1()
        {
            InitializeComponent();
        }

        public void ShowDisksInfo()
        {
            int i = 0;
            int j = 0;
            listBox2.Items.Insert(index: j, ($"Current directory: {Directory.GetCurrentDirectory()}"));
            j++;
            listBox2.Items.Insert(index: j, ($"User name: {SystemInformation.UserName}"));
            j++;
            listBox2.Items.Insert(index: j, ($"Machine name: {Environment.MachineName}"));
            j++;

            int m = 0;
            string dirName = Environment.SystemDirectory;
            string dirName1 = Path.GetTempPath();
            string dirName2 = Directory.GetCurrentDirectory();
            DirectoryInfo dirInfo = new DirectoryInfo(dirName);
            DirectoryInfo dirInfo1 = new DirectoryInfo(dirName1);
            DirectoryInfo dirInfo2 = new DirectoryInfo(dirName2);
            listBox3.Items.Insert(index: m, ($"System directory: {Environment.CurrentDirectory = (dirName)}"));
            m++;
            listBox3.Items.Insert(index: m, ($"Name of cataloge: {dirInfo.Name}"));
            m++;
            listBox3.Items.Insert(index: m, ($"Full mame of cataloge: {dirInfo.FullName}"));
            m++;
            listBox3.Items.Insert(index: m, ($"Creation time of cataloge: {dirInfo.CreationTime}"));
            m++;
            listBox3.Items.Insert(index: m, ($"Root of cataloge: {dirInfo.Root}"));
            m++;
            listBox3.Items.Insert(index: m, (" "));
            m++;
            listBox3.Items.Insert(index: m, ($"Temporary directory: {Environment.CurrentDirectory = (dirName1)}"));
            m++;
            listBox3.Items.Insert(index: m, ($"Name of cataloge: {dirInfo1.Name}"));
            m++;
            listBox3.Items.Insert(index: m, ($"Full mame of cataloge: {dirInfo1.FullName}"));
            m++;
            listBox3.Items.Insert(index: m, ($"Creation time of cataloge: {dirInfo1.CreationTime}"));
            m++;
            listBox3.Items.Insert(index: m, ($"Root of cataloge: {dirInfo1.Root}"));
            m++;
            listBox3.Items.Insert(index: m, (" "));
            m++;
            listBox3.Items.Insert(index: m, ($"Current directory: {Environment.CurrentDirectory = (dirName2)}"));
            m++;
            listBox3.Items.Insert(index: m, ($"Name of cataloge: {dirInfo2.Name}"));
            m++;
            listBox3.Items.Insert(index: m, ($"Full mame of cataloge: {dirInfo2.FullName}"));
            m++;
            listBox3.Items.Insert(index: m, ($"Creation time of cataloge: {dirInfo2.CreationTime}"));
            m++;
            listBox3.Items.Insert(index: m, ($"Root of cataloge: {dirInfo2.Root}"));

            DriveInfo[] allDrives = DriveInfo.GetDrives();
            foreach (DriveInfo d in allDrives)
            {
                listBox1.Items.Insert(index: i, ($"Drive {d.Name} File type: {d.DriveType}"));
                if (d.IsReady == true)
                {
                    i++;
                    listBox1.Items.Insert(index: i, ($"Volume label: {d.VolumeLabel}"));
                    i++;
                    listBox1.Items.Insert(index: i, ($"File system: {d.DriveFormat}"));
                    i++;
                    listBox1.Items.Insert(index: i, ($"Root directory: {d.RootDirectory}"));
                    i++;
                    listBox1.Items.Insert(index: i, ($"Available space to current user: {d.AvailableFreeSpace / 1024 / 1024 / 1024} Gbytes"));
                    i++;
                    listBox1.Items.Insert(index: i, ($"Total available space: {d.TotalFreeSpace / 1024 / 1024 / 1024} Gbytes"));
                    i++;
                    listBox1.Items.Insert(index: i, ($"Total size of drive: {d.TotalSize / 1024 / 1024 / 1024} Gbytes"));
                    i++;
                    listBox1.Items.Insert(index: i, ($"System memory: {ramCounter.NextValue().ToString()} Mbytes"));
                    i++;
                    listBox1.Items.Insert(index: i, (" "));
                }
                i++;


            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            ShowDisksInfo();
        
        }
        Thread thread2;

        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        [Obsolete]
        public void MonitorDirAndFile(string path)
        {
            using (FileSystemWatcher watcher = new FileSystemWatcher())
            {
                watcher.Path = path;

                watcher.NotifyFilter = NotifyFilters.LastAccess
                                 | NotifyFilters.LastWrite
                                 | NotifyFilters.FileName
                                 | NotifyFilters.DirectoryName;

                watcher.Filter = "";

                watcher.Changed += OnChanged;
                watcher.Created += OnChanged;
                watcher.Deleted += OnChanged;
                watcher.Renamed += OnRenamed;

                watcher.EnableRaisingEvents = true;

                watcher.WaitForChanged(WatcherChangeTypes.All);

                thread2.Suspend();

                thread2.Start();
            }
        }
        private void Button1_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                thread2 = new Thread(() => MonitorDirAndFile(folderBrowserDialog.SelectedPath));
                thread2.Start();
            }
        }
        private void OnRenamed(object sender, RenamedEventArgs e)
        {
            string path = @"D:\Uni\labs\SP\Lab03";
            using (StreamWriter sw = File.AppendText(path))
            {
                sw.WriteLine($"File: {e.OldFullPath} renamed to {e.FullPath}");
            }
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            listBox3.Items.Clear();
            ShowDisksInfo();
        }

        private void OnChanged(object sender, FileSystemEventArgs e)
        {
            string path = @"C:\Users\ASUS\source\repos\2course\SystemProgramming\Lab03\log.txt";
            using (StreamWriter sw = File.AppendText(path))
            {
                sw.WriteLine($"File: {e.FullPath} {e.ChangeType}");
            }
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            listBox3.Items.Clear();
            ShowDisksInfo();
        }
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {

            try
            {
                thread2.Resume();
                thread2.Abort();
            }
            catch
            {
            }
           


        }
    }
}

